supdevincigitlab Rattrapage


Web app choisie Redmine
Redmine est une application web libre de gestion de projets, développée en Ruby sur la base du framework Ruby on Rails. C'est un peu l'équivalent de JIRA mais en open source. Elle utilise une base de données MySql.

On peut créer un compte et s'authentifier.

Pipeline: on a 2 jobs
    - un job pour builder l'image
    - un job pour déployer sur le serveur digitalocean disponible à l'adresse http://137.184.193.100/

Etape 1 Build:
    - On a construit une image de redmine et de la base de données Mysql qu'on stocke dans le container registry de gitlab.

Etape 2 Deploy:
    - On se connecte sur le serveur digitalocean puis on se connecte sur le container registry afin de récupérer l'image
    - Une fois l'image récupérée on demarre redmine sur le serveur digitalocean
    - Une fois démarré nous pouvons y accéder avec l'url: http://137.184.193.100/

Lancement de la pipeline et connexion au site
    - se connecter au repos depuis gitlab
    - Modifier le readme
    - committer directement sur la branche master
    - la pipeline se déclenche automatiquement
    - une fois que les jobs sont executés avec succès, vous pouvez accéder à l'url: http://137.184.193.100/ pour visualiser le site déployé sur la VM en ligne

Possible à faire sur le site
    - Création de compte
    - Connexion
    - compte admin user: admin password: admin

VM:
digitalocean (Ubuntu + docker)
IP Address: 137.184.193.100
Password: p@ssW0rd
pour se connecter : ssh root@137.184.193.100

Après le déploiement aller sur l'url ci-dessous pour consulter l'application
http://137.184.193.100/
